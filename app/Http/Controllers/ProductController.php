<?php

namespace App\Http\Controllers;

use App\Http\Resources\Product\ProductResource;
use App\product_detail;
use http\Env\Response;
use Illuminate\Http\Request;
use DB;
use mysql_xdevapi\Exception;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



        public function productDetails(Request $request)
    {

        if (count($request->all()) > 0 && !empty($request->all())){
            $query = "select * from product_details where ";
            $requests = $request->all();
            $count =0;
            foreach ($requests as $key => $req){
                if($count == 0){
                    if($key == 'price'){
                   $price_range =  $this->priceRange($req,0);
                   $query .=  $price_range;
                    }else{
                        if(!empty($key))  $query .= " $key =  '$req' " ;
                    }
                    $count++;
                }else{
                    if(!empty($key))  $query .= " and $key = '$req'" ;
                }
            }
            try{
                $pro=   DB::select($query);
               if(count($pro) ==0){
                   return response()->json(["data" => "No Data Found" , "status" => "200"]);
               }

            }catch (\Illuminate\Database\QueryException $ex){
                return response()->json(["message"=>"Something went wrong","status"=>'400'],400);
            }

            $product = product_detail::hydrate($pro);

            return ProductResource::collection($product);
        }else{
            return ProductResource::collection(product_detail::all());



    }
    }

    public function priceRange($data,$count=1){
        $price_range = array('5k'=>'5000','10k'=>'10000','15k'=>'150000','20k'=>'20000','25k'=>'25000','30k'=>'30000');
        $query = null;
        if($count){
            $query = "and ";
        }
        $price_set = explode('-',$data);
                if(count($price_set) > 1){
                    $price_range1 = $price_range[$price_set[0]];
                    $price_range2 = $price_range[$price_set[1]];
                    $query .= " price < $price_range1";
                    $query .= " and price > $price_range2";
                }else{
                    $query .= " price < $price_range[$data]";
                }

            return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
