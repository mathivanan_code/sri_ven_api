<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Legger;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function leggerSubmit(Request $request){
        $legger = new Legger();

        $getleeger_id = Legger::where('loan_no', $request->loanNumber)->first();
        if($getleeger_id){
            return response()->json(['status' => false, 'statusString' => 'Legger Loan id  already present'], 200);
        }

        $legger->legger_no = $request->leggerNumber;
        $legger->loan_no = $request->loanNumber;
        $legger->loan_date = $request->loanDate;
        $legger->loan_due_date = $request->loanDueDate;
        $legger->due_amount = $request->dueAmount;
        $legger->no_installament = $request->noInstallment;
        $legger->hirename = $request->hireName;
        $legger->hire_mob_no = $request->hireMobileNo;
        $legger->hire_alt_mob_no = $request->hireAlterNumber;
        $legger->save();
        return response()->json(['status' => true, 'statusString' => 'Legger created Successfull'], 200);

    }

    public function leggerList(Request $request){

        if(!empty($request->list_id)){
         $legger = Legger::where('loan_date',$request->list_id)->get();
        }else{
            $legger =  Legger::get();
        }
        return response()->json(['status' => true, 'statusString' => 'Legger created Successfull','lists'=>$legger], 200);
    }

}
