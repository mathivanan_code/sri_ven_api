<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;


class basicauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::onceBasic()){
            return response()->json(['message'=>'Invalid Crendential','status'=>400],400);
        }else{
            return $next($request);
        }

    }
}
