<?php

use Illuminate\Database\Seeder;

use App\product_detail;
class Product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dump_products = array(
            array('sku456','Earrings','10000','A','Female','22K'),
            array('sku457','Earrings','5000','A','Female','22k'),
            array('sku458','Earrings','3000','A','Female','22K'),
            array('sku459','Earrings','13000','B','Female','22k'),
            array('sku460','Earrings','14000','B','Female','22K'),
            array('sku461','Earrings','22000','B','Female','24k'),
            array('sku462','Earrings','21000','A','Female','24K'),
            array('sku463','Earrings','19000','A','Female','22k'),
            array('sku464','Earrings','18000','B','Female','22K'),
            array('sku465','Earrings','17000','A','Female','22k'),
            array('sku466','Earrings','9000','A','Female','22K'),
            array('sku467','Earrings','12000','A','Female','22k'),
            array('sku468','Earrings','1100','A','Female','22K'),
            array('sku876','Bracelets','13410','C','Female','22K'),
            array('sku877','Bracelets','17387',	'A','Female','22K'),
            array('sku878','Bracelets','20616',	'A','Male','22K'),
            array('sku879','Bracelets','24499',	'A','Male','22K'),
            array('sku880','Bracelets','28295',	'B','Female','22K'),
            array('sku881','Bracelets','28581',	'A','Female','22K'),
            array('sku882','Bracelets','22986',	'C','Female','22K'),
            array('sku883','Bracelets','12225',	'A','Female','22K'),
            array('sku884','Bracelets','25073',	'A','Female','22K'),
            array('sku885','Bracelets','11651',	'A','Female','22K'),
            array('sku886','Bracelets','17782',	'A','Female','22K'),
            array('sku987','Chain','22072','A','Female','22K'),
            array('sku988','Chain','14523','A','Female','22K'),
            array('sku989','Chain','28209','A','Female','22K'),
            array('sku990','Chain','23947','B','Female','22K'),
            array('sku991','Chain','14533','A','Male','22K'),
            array('sku992','Chain','14710','A','Female','22K'),
            array('sku993','Chain','28075','D','Male','22K'),
            array('sku994','Chain','22758','A','Female','22K'),
            array('sku995','Chain','20837','C','Female','22K'),
            array('sku996','Chain','23495','A','Female','22K'),
            array('sku997','Chain','11141','A','Female','22K'),
            array('sku998','Chain','25553','A','Female','22K'),
            array('sku999','Chain','24126','A','Female','22K'),
            array('sku1000','Chain','26793','A','Female','22K'),
            array('sku1001','Chain','20627','A','Female','22K')
        );
        foreach ($dump_products as $dump_product){
            $product_detail = new product_detail;
            $product_detail->sku = $dump_product[0];
            $product_detail->type = $dump_product[1];
            $product_detail->price = $dump_product[2];
            $product_detail->collection = $dump_product[3];
            $product_detail->gender = $dump_product[4];
            $product_detail->karatage = $dump_product[5];
            $product_detail->save();
        }

    }
}
