<?php

use Illuminate\Database\Seeder;

use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email ='test@gmail.com';
        $name ='test@gmail.com';
        $pass ='$2y$10$E0aeLhnWMYrBbpmmArxPoOaGiQ3wWif7I66f2HZKStYPr7Ddvf9y2';

        $user = new User;
        $user->name = $name;
        $user->email = $email;
        $user->password = $pass;
        $user->save();

    }
}
