<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leggers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('legger_no');
            $table->string('loan_no');
            $table->date('loan_date');
            $table->date('loan_due_date');
            $table->string('due_amount');
            $table->string('no_installament');
            $table->string('hirename');
            $table->string('hire_mob_no');
            $table->string('hire_alt_mob_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leggers');
    }
}
