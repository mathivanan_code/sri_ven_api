    <?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::resource('/product-details','ProductController')->middleware('auth:api');
Route::get('/product_details','ProductController@productDetails')->middleware('auth.basic');




    Route::post('/legger-submit', 'HomeController@leggerSubmit');
    Route::get('/legger-list/{list_id?}', 'HomeController@leggerList');


    Route::get('/hellopostapi', function() {
        return json_encode( 'we are getting POST response');
    });
